/*#include <fstream>
#include <string>
#include <iostream>*/

#include "i2c_t3.h"

#define OBJECT_LIST_SIZE 10
#define SCAN_TIME 500.0f
#define DATA_SCAN_MESSAGE_SIZE 3000
#define TEENSY3_5_ADDR 0x02

class Object {
	public:
		float Distance;
		float PreviousDistance;
		float Velocity;
		float CenterAngle;
		float KalmanDis;
		float MeasureError;
		float CurrentDistancePrediction;
		float PreviousDistancePrediction;
		float DistancePredictionError;

		Object() {
			Distance = 0.000f;
			CenterAngle = 0.000f;
			KalmanDis = 0.500f;
			MeasureError = 4.00f;
			CurrentDistancePrediction = 0.00f;
			PreviousDistancePrediction = 0.00f;
			DistancePredictionError = 10.00f;
		}
		Object(float dis, float angle) {
			Distance = dis;
			CenterAngle = angle;
			KalmanDis = 0.500f;
			MeasureError = 4.00f;
			CurrentDistancePrediction = 0.00f;
			PreviousDistancePrediction = 0.00f;
			DistancePredictionError = 10.00f;
		}
		bool operator==(Object obj) {
			if ((Distance == obj.Distance) && (CenterAngle == obj.CenterAngle))
				return true;
			else
				return false;
		}
		void Update(Object);
		
};
class Scan {
public:
	float m_angle[300];
	float m_distance[300];
	Scan() {
		for (int i = 0; i < 300; i++) {
			m_distance[i] = 0.000f;
			m_angle[i] = 0.000f;
		}
	}
};

class Threat {
public:
	float width;
	float centerAngle;
	int severity;
    Threat(float wid, float angle, int sev){
        width = wid;
        centerAngle = angle;
        severity = sev;
    }
    Threat(){
        width = 0.00f;
        centerAngle = 0.00f;
        severity = 0;
    }
    bool operator!=(int num){
        if((width != (float)num) && (centerAngle != (float)num) && (severity != num))
            return true;
        else return false;
    }
};

void addObject(Object * List, float, float);
void requestEvent(void);
void receiveEvent(size_t numOfBytes);
void threatFound(Threat);
Object NewObjects[OBJECT_LIST_SIZE] = { Object(0.000f, 0.000f) };
Object ExistingObjects[OBJECT_LIST_SIZE] = { Object(0.000f, 0.000f) };
Threat ThreatArray[OBJECT_LIST_SIZE] = { Threat()};
String GetPacket();
Scan ParseDataPacket(String);
void loop();
bool received = false;
String DataPacket;



void setup(){
    Wire2.begin(I2C_SLAVE, TEENSY3_5_ADDR, I2C_PINS_3_4, I2C_PULLUP_EXT, 400000, I2C_OP_MODE_ISR);
    Wire2.onReceive(receiveEvent);
	Wire2.onRequest(requestEvent);
    Serial.begin(115200);
}
void loop() {
	//wait for a new scan.
    if (!received)
        return;
    received = false;
	Scan CurrentScan = ParseDataPacket(DataPacket);
	float previousDistance = 0.0f;
	int counter = 0;
	int j = 0;
	for (int i = 0; i < 300; i++) {
		float temp = CurrentScan.m_distance[i];
		int startObjectIndex = 0;
		if ((temp == 0) || (temp == 1))
			continue;
		previousDistance = temp;// CurrentScan.m_distance[i++];
		startObjectIndex = i;
		float SumOfDistances = 0.000f;
		while (fabs(temp - previousDistance) <= 20.0f) {
			SumOfDistances += temp;
			counter++;
			j++;
			temp = CurrentScan.m_distance[i + j];
		}
		i = i + j;
		if (counter >= 30) {
			//object found
			addObject(NewObjects, CurrentScan.m_angle[startObjectIndex] + CurrentScan.m_angle[(i - startObjectIndex) / 2], SumOfDistances / counter);
			//i += counter;
		}
		counter = 0;
	}
	bool ExistingObjectsEmpty = true;
	for (int j = 0; j < OBJECT_LIST_SIZE; j++) {
		if (!(ExistingObjects[j] == Object(0.00f, 0.00f))) {
			ExistingObjectsEmpty = false;
			break;
		}
	}
	if (ExistingObjectsEmpty) {
		for (int i = 0; i < OBJECT_LIST_SIZE; i++) {
			ExistingObjects[i] = NewObjects[i];
		}
		for (int i = 0; i < OBJECT_LIST_SIZE; i++) {
			NewObjects[i] = Object(0.000f, 0.000f);
		}
		return;
	}
	//now we check the existing objects with the new objects and see if there is anything to keep track of
	for (int i = 0; i < OBJECT_LIST_SIZE; i++) {
		for (int j = 0; j < OBJECT_LIST_SIZE; j++) {
			if ((ExistingObjects[i].Distance == 0.00f) || (NewObjects[j].Distance == 0.0f))
				break;
			if (fabs(ExistingObjects[i].Distance - NewObjects[j].Distance) <= 20.0f) {
				ExistingObjects[i].Update(NewObjects[j]);
				NewObjects[i] = Object(0.00f, 0.00f);
			}
		}
	}
	//now we gotta alert for threats babbyyyy
	float TimeUntilReachedDistancePrediction = 0.00f;
	for (int i = 0; i < OBJECT_LIST_SIZE; i++) {
		if (ExistingObjects[i].CurrentDistancePrediction <= 300) {
			TimeUntilReachedDistancePrediction = ((0 - ExistingObjects[i].CurrentDistancePrediction) / ExistingObjects[i].Velocity); //0 is our final position
			if (TimeUntilReachedDistancePrediction < 0) {
				//its going away
			}
			else if (TimeUntilReachedDistancePrediction < 1000) {
				//Threat level immediate
				threatFound(Threat(10.0f, ExistingObjects[i].CenterAngle, 2));
			}
			else if (TimeUntilReachedDistancePrediction < 3000) { //ms

			}
			else if (TimeUntilReachedDistancePrediction < 5000){

			}

		}
	}
}
Scan ParseDataPacket(String input) {
	//to do do this for the byte array rather than a string
	Scan temp;
	int prevCommaLocation = 0;
	int nextCommaLocation = 0;
	String dis = "";
	for (int i = 0; i < 300; i++) {
		nextCommaLocation = input.indexOf(',', prevCommaLocation);
		if (nextCommaLocation == -1) {
			nextCommaLocation = input.indexOf(';');
			dis = input.substring(prevCommaLocation, nextCommaLocation);
			temp.m_angle[i] = i;
			temp.m_distance[i] = atof(dis.c_str());
			break;
		}
		dis = input.substring(prevCommaLocation, nextCommaLocation);
		temp.m_angle[i] = i;
		temp.m_distance[i] = atof(dis.c_str());
		prevCommaLocation = nextCommaLocation + 1;
	}
	return temp;
}
void receiveEvent(size_t numOfBytes){
    uint8_t databuf[DATA_SCAN_MESSAGE_SIZE];
    size_t i;
    for (i = 0; i < numOfBytes; i++){
        if(i < DATA_SCAN_MESSAGE_SIZE - 1)
            databuf[i] = Wire2.readByte();
    }
    databuf[i] = ';';
    char temp[DATA_SCAN_MESSAGE_SIZE];
    for (i = 0; i < DATA_SCAN_MESSAGE_SIZE; i++){
        temp[i] = (char) databuf[i];
    }
    String newPacket = String(temp);
    DataPacket = newPacket.substring(0, newPacket.indexOf(';') + 1);
    received = true;
}
void requestEvent(){
    String Threats = "";
    for (int i = 0; i < OBJECT_LIST_SIZE; i++){
        if(ThreatArray[i] != 0){
            Threats += String(ThreatArray[i].centerAngle) + "," + String(ThreatArray[i].width) + "," + String(ThreatArray[i].centerAngle) + ":";
        }
    }
	if(Threats == "")
		return;
	for (size_t j = 0; j < Threats.length(); j++){
        Wire2.write(Threats[j]);
    }
}
void addObject(Object * List, float angle, float distance) {
	for (int i = 0; i < OBJECT_LIST_SIZE; i++) {
		if ((List[i].CenterAngle == 0.00f) && (List[i].Distance == 0.00f)) {
			List[i].CenterAngle = angle;
			List[i].Distance = distance;
			List[i].PreviousDistancePrediction = distance * 0.8f;
			List[i].CurrentDistancePrediction = distance * 0.85f;
			break;
		}
	}
}
void Object::Update(Object obj) {
	this->PreviousDistance = this->Distance;
	this->Distance = obj.Distance;
	this->CenterAngle = obj.CenterAngle;
	this->KalmanDis = this->DistancePredictionError / (this->DistancePredictionError + this->MeasureError);
	this->CurrentDistancePrediction = this->PreviousDistancePrediction + this->KalmanDis * (this->Distance - this->PreviousDistancePrediction);
	float temp = this->DistancePredictionError;
	this->DistancePredictionError = (1.000f - this->KalmanDis) * temp;
	this->Velocity = (this->Distance - this->PreviousDistance) / SCAN_TIME;
}
void threatFound(Threat threat){
	for (int i = 0; i < OBJECT_LIST_SIZE; i++){
		if(!(ThreatArray[i] != 0)){
			ThreatArray[i].centerAngle = threat.centerAngle;
			ThreatArray[i].width = threat.width;
			ThreatArray[i].severity = threat.severity;
		}
	}
}