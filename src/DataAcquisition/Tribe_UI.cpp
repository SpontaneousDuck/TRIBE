#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include "Tribe_UI.h"

Tribe_UI::Tribe_UI() {
    pinMode(LED_RING_PIN, OUTPUT);
    pixels = Adafruit_NeoPixel(NUM_PIXELS, LED_RING_PIN, NEO_GRB + NEO_KHZ800);
    pixels.begin();
    pixels.setBrightness(3);
}

void Tribe_UI::LightLEDs(int centerAngle, int width, int severity) {
    // pixels.setPixelColor(12, pixels.Color(255, 0,0));
    // pixels.show();
    int LEDToLight = 0;
    int R = 0;
    int G = 0;
    int B = 0;

    if (centerAngle >= 0 && centerAngle <= 8)
        LEDToLight = 12;
    else if (centerAngle <= 17)
        LEDToLight = 13;
    else if (centerAngle <= 25)
        LEDToLight = 14;
    else if (centerAngle <= 33)
        LEDToLight = 15;
    else if (centerAngle <= 41)
        LEDToLight = 16;
    else if (centerAngle <= 49)
        LEDToLight = 17;
    else if (centerAngle <= 57)
        LEDToLight = 18;
    else if (centerAngle <= 65)
        LEDToLight = 19;
    else if (centerAngle <= 73)
        LEDToLight = 20;
    else if (centerAngle <= 81)
        LEDToLight = 21;
    else if (centerAngle <= 89)
        LEDToLight = 22;
    else if (centerAngle <= 97)
        LEDToLight = 23;

    switch(severity){
        case 0:
            Serial.println("Green");
            R = 0;
            G = 255;
            B = 0;
            break;
        case 1:
            Serial.println("yello");
            R = 255;
            G = 255;
            B = 0;
            break;
        case 2:
            Serial.println("red");
            R = 255;
            G = 0;
            B = 0;
            break;
        case 4:
            LEDClearAll();
            return;
        default:
            break;
        
    }
    int numOfLEDsLit = 1;
    if (width >= 10){
        numOfLEDsLit = 3;
    }

    int temp = (numOfLEDsLit % 2 == 1) ? (numOfLEDsLit - 1) / 2 : numOfLEDsLit / 2;
    LEDToLight -= temp;

    for (int i = LEDToLight; i < (LEDToLight + numOfLEDsLit); i++){
        pixels.setPixelColor(i, pixels.Color(R, G, B));
    }
    pixels.setBrightness(5);
    pixels.show();


}

void Tribe_UI::LEDClearAll(){
    for (int i = 0; i < 24; i++){
        pixels.setPixelColor(i, pixels.Color(0, 0 ,0));
    }
}