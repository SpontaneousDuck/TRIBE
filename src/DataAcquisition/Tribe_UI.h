
#ifndef Tribe_UI_h
#define Tribe_UI_h

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

#define NUM_PIXELS          24
#define LED_RING_PIN        8

class Tribe_UI
{
    public: 
        Adafruit_NeoPixel pixels;

        Tribe_UI();
        void LightLEDs(int centerAngle, int width, int severity);
        void LEDClearAll();
};

#endif