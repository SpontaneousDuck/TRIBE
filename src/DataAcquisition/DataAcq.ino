//Code Developed by Rob Cheetham with Ismael Veyna and Kenny Witham 

#include "LIDARLite.h"
#include "i2c_t3.h"
#include "Tribe_UI.h"

#define DEBUG               0 //set to 1 for debugging the messages over serial.
#define AVERAGE_TIME_COUNT  500
#define MOTOR_PIN_1         3
#define MOTOR_PIN_2         4
#define TEENSY3_5_ADDR      0x02
#define WALL_LENGTH         20


IntervalTimer myTimer;
IntervalTimer threatRequest;
LIDARLite lidar;

int dis;
int motorState = LOW;
String DataScanPacket = "";
int endCount = 0;
Tribe_UI neopixel;

void swapMotor();
 
void setup(){
    //initialize control pins
    pinMode(MOTOR_PIN_1, OUTPUT); 
    pinMode(MOTOR_PIN_2, OUTPUT);
    pinMode(LED_BUILTIN, OUTPUT);
    Serial.begin(115200);

    //initialize I2C bus
    Wire.begin(I2C_MASTER, 0x00, I2C_PINS_18_19, I2C_PULLUP_EXT, 400000, I2C_OP_MODE_ISR);
    
    //Start LIDAR measurements
    lidar.begin(1, false);

    //reserve a known length for data storage to reduce realocation 
    //DataScanPacket.reserve(600);

    //Wait if debugging to allow for serial open
    #if DEBUG
        delay(5000);
    #endif

    //initialize motor
    digitalWrite(3, HIGH); //keep going right
    digitalWrite(4, LOW);
    
    while(lidar.distance(0) < WALL_LENGTH + 10){
        #if DEBUG
            Serial.println("In wall");
        #endif
    }
}

void loop(){
    //take a measurement
    dis = lidar.distance(0);
    //detect end of scan and swap
    if (dis != 1 && dis < WALL_LENGTH){
        endCount++;
        if (endCount >= 2){
            swapMotor();
            endCount = 0;
        }
        dis = 1;
    } else {
        endCount = 0;
    }

     //add reading to data packet if not at end
    if (motorState){
        if(DataScanPacket == "") {
            DataScanPacket = String(dis) + DataScanPacket;
        }
        else{
            DataScanPacket = String(dis) + "," + DataScanPacket;
        }
    }
    else{
        if(DataScanPacket != "") {
            DataScanPacket += ",";
        }
        DataScanPacket += dis;
    }

}

void request(){
    // Print message
    Wire.finish();

    #if DEBUG
        Serial.print("Reading from Slave: ");
    #endif

    Wire.requestFrom(TEENSY3_5_ADDR, 1200);

    // Check if error occured
    if(Wire.getError()) {
        #if DEBUG
            Serial.print(Wire.getError());  
            Serial.print("-FAIL\n");
            digitalWrite(13, LOW);
        #endif
    }
    else
    {   
        #if DEBUG
            Serial.print("Success\n");
        #endif

        String LEDPacket = "";
        LEDPacket.reserve(1200);
        byte val;
        while(Wire.available()){
            val = Wire.read();
            LEDPacket = LEDPacket + (char)val;
        }
        LEDPacket = LEDPacket.substring(0, LEDPacket.indexOf(';'));

        #if DEBUG
            Serial.print("Packet: ");  
            Serial.println(LEDPacket);
        #endif

        ParseLEDPacket(LEDPacket);

        LEDPacket = "";
    }

    #if DEBUG
        digitalWrite(13, LOW);
    #endif
}

void sendDataScanPacket(){
    #if DEBUG
        digitalWrite(13, HIGH);
    #endif
    Wire.beginTransmission(TEENSY3_5_ADDR); //address of teensy 3.5
    unsigned int len = DataScanPacket.length();
    len = len>600?600:len;

    #if DEBUG
        Serial.print("Sending: ");
        Serial.println(len);
    #endif

    for (unsigned int i = 0; i < len; i++){
        Wire.write(DataScanPacket[i]);

        #if DEBUG
            Serial.print(DataScanPacket[i]);
        #endif
    }

    Wire.endTransmission(TEENSY3_5_ADDR);
    DataScanPacket = ""; //reset the DataScanPacket Buffer
}

//TODO: verify parsing packet works

void ParseLEDPacket(String packet){
    int beginning = 0;
    int firstDelimiter = 0;
    int secondDelimiter = 0;
    for (int i = 0; i < (int) packet.length(); i++){
        if (packet.charAt(i) == ','){
            if ((i > firstDelimiter) && (firstDelimiter == 0)){
                firstDelimiter = i;
            }
            else if ((i > firstDelimiter) && (firstDelimiter != 0)){
                secondDelimiter = i;
            }
        }
        if (packet.charAt(i) == ':') {
            String center = packet.substring(beginning, firstDelimiter);
            String width = packet.substring(firstDelimiter + 1, secondDelimiter);
            String severity = packet.substring(secondDelimiter + 1, i);
            
            neopixel.LEDClearAll();
            neopixel.LightLEDs(center.toInt(), width.toInt(), severity.toInt());
            beginning = i+1;
        }
    }
}

void swapMotor(){
    if (motorState){
        motorState = LOW;
        //switch to right
        digitalWrite(MOTOR_PIN_1, HIGH);
        digitalWrite(MOTOR_PIN_2, LOW);
    }
    else{
        motorState = HIGH;
        //switch to left
        digitalWrite(MOTOR_PIN_1, LOW);
        digitalWrite(MOTOR_PIN_2, HIGH);
    }
    DataScanPacket = DataScanPacket + ';';
    //Serial1.println(DataScanPacket);
    int startTime = millis();
    sendDataScanPacket();
    request();
    while((millis() - startTime) < 200); //change this value for whatever
}