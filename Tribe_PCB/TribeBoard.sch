EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:teensy
LIBS:L283D
LIBS:TribeBoard-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "TRIBE Main Board"
Date "2017-07-13"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L L293D CR1
U 1 1 59661EAF
P 1150 2600
F 0 "CR1" H 1950 2750 45  0000 L BNN
F 1 "L293D" H 1900 2650 45  0000 L BNN
F 2 "L283D:-POWERDIP-16" H 1230 2600 20  0001 C CNN
F 3 "" H 1200 2450 60  0001 C CNN
	1    1150 2600
	1    0    0    -1  
$EndComp
$Comp
L D D1
U 1 1 59661F82
P 2150 4000
F 0 "D1" H 2150 4100 50  0000 C CNN
F 1 "D" H 2150 3900 50  0000 C CNN
F 2 "Diodes_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 2150 4000 50  0001 C CNN
F 3 "" H 2150 4000 50  0001 C CNN
	1    2150 4000
	1    0    0    1   
$EndComp
$Comp
L D D2
U 1 1 5966200F
P 2150 4300
F 0 "D2" H 2150 4400 50  0000 C CNN
F 1 "D" H 2150 4200 50  0000 C CNN
F 2 "Diodes_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 2150 4300 50  0001 C CNN
F 3 "" H 2150 4300 50  0001 C CNN
	1    2150 4300
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X06 J3
U 1 1 59662102
P 6250 6150
F 0 "J3" H 6250 6500 50  0000 C CNN
F 1 "LIDAR" V 6350 6150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 6250 6150 50  0001 C CNN
F 3 "" H 6250 6150 50  0001 C CNN
	1    6250 6150
	1    0    0    -1  
$EndComp
$Comp
L CP C1
U 1 1 5966221A
P 3400 1050
F 0 "C1" H 3425 1150 50  0000 L CNN
F 1 "680uF" H 3425 950 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D4.0mm_P2.00mm" H 3438 900 50  0001 C CNN
F 3 "" H 3400 1050 50  0001 C CNN
	1    3400 1050
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 J2
U 1 1 596622C1
P 2050 4700
F 0 "J2" H 2050 4850 50  0000 C CNN
F 1 "Motor" V 2150 4700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 2050 4700 50  0001 C CNN
F 3 "" H 2050 4700 50  0001 C CNN
	1    2050 4700
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 J4
U 1 1 59662368
P 6200 4950
F 0 "J4" H 6200 5150 50  0000 C CNN
F 1 "NeoPixel" V 6300 4950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 6200 4950 50  0001 C CNN
F 3 "" H 6200 4950 50  0001 C CNN
	1    6200 4950
	1    0    0    -1  
$EndComp
Text Label 1500 1500 0    60   ~ 0
GND
Text Label 8750 6200 0    60   ~ 0
VHigh
$Comp
L NCP1117ST50T3G U3
U 1 1 59677AB5
P 2400 900
F 0 "U3" H 2400 1125 50  0000 C CNN
F 1 "NCP1117ST50T3G" H 2400 1050 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-223" H 2450 650 50  0001 L CNN
F 3 "" H 2400 900 50  0001 C CNN
	1    2400 900 
	1    0    0    -1  
$EndComp
Text Label 1550 1400 0    60   ~ 0
VI
$Comp
L GND #PWR01
U 1 1 5967E85D
P 1150 5950
F 0 "#PWR01" H 1150 5700 50  0001 C CNN
F 1 "GND" H 1150 5800 50  0000 C CNN
F 2 "" H 1150 5950 50  0001 C CNN
F 3 "" H 1150 5950 50  0001 C CNN
	1    1150 5950
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 5967E68B
P 1600 5650
F 0 "#FLG02" H 1600 5725 50  0001 C CNN
F 1 "PWR_FLAG" H 1600 5800 50  0000 C CNN
F 2 "" H 1600 5650 50  0001 C CNN
F 3 "" H 1600 5650 50  0001 C CNN
	1    1600 5650
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG03
U 1 1 5967E42E
P 1150 5650
F 0 "#FLG03" H 1150 5725 50  0001 C CNN
F 1 "PWR_FLAG" H 1150 5800 50  0000 C CNN
F 2 "" H 1150 5650 50  0001 C CNN
F 3 "" H 1150 5650 50  0001 C CNN
	1    1150 5650
	1    0    0    -1  
$EndComp
Text Label 1600 5800 0    60   ~ 0
VI
$Comp
L +8V #PWR04
U 1 1 596826CF
P 1600 5950
F 0 "#PWR04" H 1600 5800 50  0001 C CNN
F 1 "+8V" H 1600 6090 50  0000 C CNN
F 2 "" H 1600 5950 50  0001 C CNN
F 3 "" H 1600 5950 50  0001 C CNN
	1    1600 5950
	1    0    0    1   
$EndComp
$Comp
L Teensy3.5 U2
U 1 1 59661562
P 8300 3350
F 0 "U2" H 8300 950 60  0000 C CNN
F 1 "Teensy3.5" H 8300 1050 60  0000 C CNN
F 2 "teensy:Teensy35_36" H 8300 3350 60  0001 C CNN
F 3 "" H 8300 3350 60  0000 C CNN
	1    8300 3350
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5976D599
P 6000 2200
F 0 "R1" V 6080 2200 50  0000 C CNN
F 1 "2.2k" V 6000 2200 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 5930 2200 50  0001 C CNN
F 3 "" H 6000 2200 50  0001 C CNN
	1    6000 2200
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5976DF96
P 6300 2200
F 0 "R2" V 6380 2200 50  0000 C CNN
F 1 "2.2k" V 6300 2200 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 6230 2200 50  0001 C CNN
F 3 "" H 6300 2200 50  0001 C CNN
	1    6300 2200
	1    0    0    -1  
$EndComp
$Comp
L Teensy3.2 U1
U 1 1 5976E882
P 4350 2800
F 0 "U1" H 4350 1200 60  0000 C CNN
F 1 "Teensy3.2" H 4350 1300 60  0000 C CNN
F 2 "teensy:Teensy30_31_32_LC" H 4350 2000 60  0001 C CNN
F 3 "" H 4350 2000 60  0000 C CNN
	1    4350 2800
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X04 J1
U 1 1 5977E41A
P 1250 1550
F 0 "J1" H 1250 1800 50  0000 C CNN
F 1 "CONN_01X04" V 1350 1550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 1250 1550 50  0001 C CNN
F 3 "" H 1250 1550 50  0001 C CNN
	1    1250 1550
	-1   0    0    1   
$EndComp
Text Label 1700 1700 0    60   ~ 0
SDA
Text Label 1700 1600 0    60   ~ 0
SCL
Text Label 5700 6300 0    60   ~ 0
SCL
Text Label 5650 6400 0    60   ~ 0
SDA
Wire Wire Line
	5850 2900 5850 4500
Wire Wire Line
	5850 4500 5850 5450
Connection ~ 3000 3050
Wire Wire Line
	3000 3050 2650 3050
Wire Wire Line
	1200 1900 1000 1900
Wire Wire Line
	1200 1900 1200 1900
Wire Wire Line
	1200 1900 3350 1900
Connection ~ 1150 2000
Wire Wire Line
	1150 2000 3350 2000
Wire Wire Line
	1150 5650 1150 5950
Wire Wire Line
	1600 5950 1600 5650
Connection ~ 850  2750
Wire Wire Line
	1300 2750 850  2750
Connection ~ 2850 2300
Wire Wire Line
	850  3450 1300 3450
Wire Wire Line
	850  2300 850  2750
Wire Wire Line
	850  2750 850  3450
Wire Wire Line
	2850 2300 850  2300
Connection ~ 2850 900 
Wire Wire Line
	2850 2750 2650 2750
Wire Wire Line
	2850 900  2850 2300
Wire Wire Line
	2850 2300 2850 2750
Connection ~ 5750 900 
Wire Wire Line
	5750 900  5750 3600
Wire Wire Line
	2400 1200 2400 1400
Wire Wire Line
	2400 1400 2400 1500
Connection ~ 9700 4550
Wire Wire Line
	2700 900  2850 900 
Wire Wire Line
	2850 900  5750 900 
Wire Wire Line
	5750 900  6000 900 
Wire Wire Line
	6000 900  6300 900 
Wire Wire Line
	6300 900  9700 900 
Wire Wire Line
	3050 2400 3050 4850
Wire Wire Line
	3050 2400 3350 2400
Connection ~ 6650 5250
Wire Wire Line
	6000 5250 6650 5250
Wire Wire Line
	6000 5050 6000 5250
Connection ~ 6650 1250
Connection ~ 6850 4650
Wire Wire Line
	6850 6200 9700 6200
Connection ~ 6850 6200
Wire Wire Line
	9700 6200 9700 4550
Wire Wire Line
	9700 4550 9700 900 
Wire Wire Line
	9300 4550 9700 4550
Wire Wire Line
	3000 1250 3400 1250
Wire Wire Line
	3400 1250 6650 1250
Wire Wire Line
	6650 1250 7300 1250
Connection ~ 1650 4750
Wire Wire Line
	1650 4000 1650 4750
Wire Wire Line
	1150 2950 1300 2950
Wire Wire Line
	1150 4750 1150 2950
Wire Wire Line
	1150 4750 1650 4750
Wire Wire Line
	1650 4750 1850 4750
Connection ~ 1750 4650
Wire Wire Line
	1200 3250 1200 4650
Wire Wire Line
	1300 3250 1200 3250
Wire Wire Line
	1200 4650 1750 4650
Wire Wire Line
	1750 4650 1850 4650
Wire Wire Line
	1750 4300 1750 4650
Connection ~ 3000 4000
Wire Wire Line
	1000 1900 1000 3350
Wire Wire Line
	1000 3350 1300 3350
Wire Wire Line
	1150 2850 1300 2850
Wire Wire Line
	3000 1250 3000 1400
Wire Wire Line
	3000 1400 3000 3050
Wire Wire Line
	3000 3050 3000 3150
Wire Wire Line
	3000 3150 3000 4000
Wire Wire Line
	3000 4000 3000 4300
Connection ~ 5850 2900
Wire Wire Line
	5500 3100 6300 3100
Wire Wire Line
	6300 3100 7150 3100
Wire Wire Line
	5650 5450 5650 6300
Wire Wire Line
	7000 2900 7000 1650
Wire Wire Line
	7000 1650 7300 1650
Wire Wire Line
	7150 3100 7150 1750
Wire Wire Line
	7150 1750 7300 1750
Wire Wire Line
	3000 4000 2300 4000
Wire Wire Line
	2000 4000 1650 4000
Wire Wire Line
	2000 4300 1750 4300
Wire Wire Line
	3000 4300 2300 4300
Wire Wire Line
	6850 4650 6850 6200
Wire Wire Line
	6850 6200 6850 6600
Wire Wire Line
	5850 5450 5650 5450
Wire Wire Line
	5500 3800 5500 4600
Wire Wire Line
	5500 4600 5500 6400
Wire Wire Line
	5500 3800 5500 3800
Wire Wire Line
	6000 2050 6000 900 
Connection ~ 6000 900 
Wire Wire Line
	6300 2050 6300 900 
Connection ~ 6300 900 
Wire Wire Line
	6000 2350 6000 2900
Connection ~ 6000 2900
Wire Wire Line
	6300 2350 6300 3100
Connection ~ 6300 3100
Wire Wire Line
	5750 3600 5350 3600
Wire Wire Line
	5850 2900 6000 2900
Wire Wire Line
	6000 2900 7000 2900
Wire Wire Line
	5500 3800 5500 3100
Wire Wire Line
	3350 3900 3250 3900
Wire Wire Line
	3250 4600 5500 4600
Connection ~ 5500 4600
Wire Wire Line
	3350 4000 3300 4000
Wire Wire Line
	3300 4000 3300 4250
Wire Wire Line
	3300 4250 3300 4500
Wire Wire Line
	3300 4500 5850 4500
Connection ~ 5850 4500
Wire Wire Line
	2650 3150 3000 3150
Connection ~ 3000 3150
Wire Wire Line
	1950 900  2100 900 
Connection ~ 2400 1400
Connection ~ 3000 1400
Wire Wire Line
	3350 1400 3350 1500
Connection ~ 3250 3900
Wire Wire Line
	3150 4250 3300 4250
Connection ~ 3300 4250
Wire Wire Line
	1450 1400 1950 1400
Wire Wire Line
	1950 1400 1950 900 
Wire Wire Line
	2400 1500 1450 1500
Wire Wire Line
	2400 1400 3000 1400
Wire Wire Line
	3000 1400 3350 1400
Wire Wire Line
	1450 1700 3250 1700
Wire Wire Line
	3250 1700 3250 3900
Wire Wire Line
	3250 3900 3250 4600
Wire Wire Line
	3150 4250 3150 1600
Wire Wire Line
	3150 1600 1450 1600
Wire Wire Line
	6850 4650 5900 4650
Wire Wire Line
	5900 4650 5900 4950
Wire Wire Line
	5900 4950 6000 4950
Wire Wire Line
	3050 4850 6000 4850
Wire Wire Line
	6650 5550 6050 5550
Wire Wire Line
	6050 5550 6050 5900
Connection ~ 6650 5550
Wire Wire Line
	5900 6600 5900 6200
Wire Wire Line
	5900 6200 6050 6200
Wire Wire Line
	5650 6300 6050 6300
Wire Wire Line
	5500 6400 6050 6400
Wire Wire Line
	6850 6600 5900 6600
Wire Wire Line
	6650 1250 6650 5250
Wire Wire Line
	6650 5250 6650 5550
Wire Wire Line
	3400 1200 3400 1250
Connection ~ 3400 1250
Wire Wire Line
	1150 2850 1150 2000
$EndSCHEMATC
